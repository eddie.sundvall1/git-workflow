# Git workflow

1.  Create feature branch from master:<br>
`git checkout master`<br>
`git checkout -b feature-branch`
2.  Modify local content and review changes:<br>
`git diff` or `git diff --word-diff` (for inline diff)
3.  Stage changes:<br>
`git add .` for all changes or `git add path/to/file.end` for single file<br>
4.  Create commit on the branch:<br>
`git commit -m "Short description"` or just `git commit` to bring up editor and create message there
5.  Repeat step 2 to 4 until feature is complete
6.  Merge feature branch to master:<br>
`git checkout master`<br>
`git merge feature-branch --no-ff`
